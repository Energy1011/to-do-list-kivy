#!/bin/env python

# WARNING: THIS PROGRAM IS NOT FINISHED COMPLETELY

"""
APP to create a ToDo List using Kivy
Copyright (C) 2016  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from kivy.app import App
from kivy.base import Builder
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.scrollview import ScrollView
from kivy.properties import StringProperty
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.checkbox import CheckBox
import os.path

class ListItem(BoxLayout):

    def __init__(self, **kvargs):
        super(ListItem, self).__init__(**kvargs)
        label = Label()
        checkbox = CheckBox()
        label.text = kvargs['text']
        self.add_widget(label)
        self.add_widget(checkbox)


class RootWidget(GridLayout):
    cols = 1
    myList = []
    todo_filname = 'todo.txt'

    def __init__(self, **kvargs):
        super(RootWidget, self).__init__(**kvargs)
        self.read_todo_file()

    def append_todo_file(self, text):
        text = text.strip()
        with open(self.todo_filname, 'a') as f:
            f.write(text+"\n")
        f.close()

    def read_todo_file(self):
        if os.path.isfile(self.todo_filname) is False:
            with open(self.todo_filname,'a') as f:
                pass
            f.close()

        with open(self.todo_filname,'r+') as f:
            line=f.readline()
            #Check iter over file items
            for line in f:
                if line != "":
                    self.add_item(line)
        f.close()

    def add_item(self, text=""):
        if text == "":
            text = self.ids.input_item.text
            self.append_todo_file(text)
        item = ListItem(text=text)
        self.ids.widget_list.add_widget(item)


class ToDoListApp(App):

    def build(self):
        return RootWidget()

if __name__ == '__main__':
    ToDoListApp().run()